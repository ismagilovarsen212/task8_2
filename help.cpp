#include "atd.h"

class Matrix{
    int n, m;
    double **mas;
    public:
        Matrix()
        {
            this->n = 1;
            this->m = 1;
            mas = new double*[n];
            mas[0] = new double[m];
            mas[0][0] = 0.0; 
        }
        Matrix(int n, int m)
        {
            this->n = n;
            this->m = m;
            mas = new double*[n];
            for(int i = 0; i < n; i++)
            {
                mas[i] = new double[m];
            }
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] = 0.0;
        }
        Matrix(double x)
        {
            this->n = 1;
            this->m = 1;
            mas = new double*[n];
            mas[0] = new double[m];
            mas[0][0] = x;
        }
        Matrix(double *x, int m)
        {
            this->n = 1;
            this->m = m;
            mas = new double* [n];
            mas[0] = new double [m];
            for(int i = 0; i < m; i++)
                mas[0][i] = x[i];
        }
        Matrix(int n, double *x)
        {
            this->n = n;
            this->m = 1;
            mas = new double*[n];
            for(int i = 0; i < n; i++)
                mas[i] = new double;
            for(int i = 0; i < n; i++)
                mas[i][0] = x[i];
        }
        Matrix(const Matrix &a)
        {
            this->n = a.n;
            this->m = a.m;
            mas = new double* [n];
            for(int i = 0; i < n; i++)
                mas[i] = new double [m];
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] = a.mas[i][j];
        }
        double& elem(int i = 0, int j = 0) 
        {
            return mas[i][j];
        }

        ~Matrix()
        {
            for(int i = 0; i < n; i++)
                delete[] mas[i];
            if(this->n > 1 && this->m > 1)
                delete[] mas;
        }
        int rows()
        {
            return n;
        }
        int columns()
        {
            return m;
        }
        void set(int i, int j, double val)
        {
            mas[i][j] = val;
        }
        Matrix &operator * (double x)
        {
            static Matrix a(n, m);
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    a.set(i, j, mas[i][j]);
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    a.elem(i, j) *= x;
            return a;
        }
        const Matrix &operator *= (double x)
        {
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] *= x;
            return *this;
        }
        Matrix &operator = (const Matrix &);
        friend std::ostream& operator<< (std::ostream &out, const Matrix &matrix);
        Matrix &operator + (const Matrix &);
        Matrix &operator += (const Matrix &);
        Matrix &operator - (const Matrix &);
        Matrix &operator -= (const Matrix &);
        Matrix &operator * (const Matrix &);
        Matrix &operator *= (const Matrix &);
        bool operator == (const Matrix &);
        bool operator != (const Matrix &);
        friend Matrix &operator - (const Matrix &);
        Matrix &operator | (const Matrix &);
        Matrix &operator / (const Matrix &);
        Matrix operator [] (int i)
        {
            if(i >= 0 && i < n)
            {
                static Matrix tmp(1, m);
                for(int j = 0; j < m; j++)
                    tmp.set(0, j, mas[i][j]);
                return tmp;
            }
            else if(i >= 0 && i < m)
            {
                static Matrix tmp(n, 1);
                for(int j = 0; j < n; j++){
                    tmp.set(j, 0, mas[j][i]);
                }
                return tmp;
            }
            else
            {
                cout << "Error of index!" << endl;
                return *this;
            }
        }
        
};

Matrix &Matrix::operator + (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        static Matrix a(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.set(i, j, mas[i][j]);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.elem(i, j) += b.mas[i][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator += (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] += b.mas[i][j];
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator - (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        static Matrix a(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.set(i, j, mas[i][j]);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.elem(i, j) -= b.mas[i][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator -= (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] -= b.mas[i][j];
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator * (const Matrix &b)
{
    if(m == b.n)
    {
        static Matrix a(n, b.m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < b.m; j++)
                for(int k = 0; k < m; k++)
                    a.elem(i, j) += mas[i][k] * b.mas[k][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator *= (const Matrix &b)
{
    if(m == b.n)
    {
        Matrix a(n, b.m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < b.m; j++)
                for(int k = 0; k < m; k++)
                    a.elem(i, j) += mas[i][k] * b.mas[k][j];
        for(int i = 0; i < n; i++)
                delete[] mas[i];
        if(this->n > 1 && this->m > 1)
            delete[] mas;
        this->m = b.m;
        mas = new double*[n];
        for(int i = 0; i < n; i++)
        {
            mas[i] = new double[m];
        }
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] = a.elem(i, j);
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator = (const Matrix &a)
        {
            if(n == a.n && m == a.m)
                for(int i = 0; i < n; i++)
                    for(int j = 0; j < m; j++)
                        mas[i][j] = a.mas[i][j];
            else
                cout << "Error of size" << endl;
            return *this;
        }

Matrix identity(int n)
{
    Matrix a(n, n);
    for(int i = 0; i < n; i++)
        a.set(i, i, 1.0);
    return a;
}
std::ostream& operator<< (std::ostream &out, const Matrix &a)
{
    for(int i = 0; i < a.n; i++)
    {
        for(int j = 0; j < a.m; j++)
            out << a.mas[i][j] << " ";
        out << " " << endl;
    }

    return out;
}

Matrix &operator - (const Matrix &a)
{
    static Matrix b(a.n, a.m);
    for(int i = 0; i < a.n; i++)
        for(int j = 0; j < a.m; j++)
            b.mas[i][j] = -a.mas[i][j];
    return b;
} 

Matrix diagonal(double *vals, int n)
{
    Matrix a(n, n);
    for(int i = 0; i < n; i++)
        a.set(i, i, vals[i]);
    return a;
}

bool Matrix::operator == (const Matrix &a)
{
    if(n == a.n && m == a.m){
        bool ams = true;
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < m; j++)
                if(abs(mas[i][j] - a.mas[i][j]) >= eps)
                {
                    ams = false;
                    break;
                }
            if(!ams)
                break;
        }
        return ams;
    }
    else
    {
        cout << "Different sizes of comparatible matrixes" << endl;
        return false;
    }
        
}

bool Matrix::operator != (const Matrix &a)
{
    if(n == a.n && m == a.m){
        bool ams = true;
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < m; j++)
                if(abs(mas[i][j] - a.mas[i][j]) >= eps)
                {
                    ams = false;
                    break;
                }
            if(!ams)
                break;
        }
        return !ams;
    }
    else
    {
        cout << "Different sizes of comparatible matrixes" << endl;
        return true;
    }
        
}

Matrix &Matrix::operator | (const Matrix &a)
{
    if(n == a.n)
    {
        static Matrix temp(n, m + a.m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m + a.m; j++)
            {
                if(j < m)
                    temp.mas[i][j] = mas[i][j];
                else
                    temp.mas[i][j] = a.mas[i][j - m];
            }
        return temp;
    }
    else
    {
        cout << "Different amount of rows" << endl;
        return *this;
    }
    
}

Matrix &Matrix::operator / (const Matrix &a)
{
    if(m == a.m)
    {
        static Matrix temp(n + a.n, m);
        for(int i = 0; i < n + a.n; i++)
            for(int j = 0; j < m; j++)
            {
                if(i < n)
                    temp.mas[i][j] = mas[i][j];
                else
                    temp.mas[i][j] = a.mas[i - n][j];
            }
        return temp;
    }
    else
    {
        cout << "Different amount of columns" << endl;
        return *this;
    }
}