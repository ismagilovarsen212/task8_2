#include "help.cpp"

int main()
{
    double x;
    int N, M, Tmp;
    cout << "Enter the sizes of Matrix #1: " << endl;
    cin >> N >> M;
    Matrix A(N, M);
    cout << "Enter the values of Matrix #1: " << endl;
    for(int i = 0; i < N; i++)
        for(int j = 0; j < M; j++)
        {
            cin >> x;
            A.set(i, j, x);
        }
    cout << "Matrix #1:" << endl;
    cout << A << endl;

    cout << "Enter the size of identity matrix: " << endl;
    cin >> Tmp;
    Matrix I = identity(Tmp);
    cout << "Identity matrix: " << endl;
    cout << I << endl;

    cout << "Enter a size of diagonal matrix: " << endl;
    cin >> Tmp;
    cout << "Enter the diagonal values: " << endl;
    double *vals = new double [Tmp];
    for(int i = 0; i < Tmp; i++)
        cin >> vals[i];
    Matrix D = diagonal(vals, Tmp);
    cout << "Diagonal matrix: " << endl;
    cout << D << endl;

    cout << "Amount of rows of Matrix #1: " << A.rows() << endl;
    cout << "Amount of columns of Matrix #1: " << A.columns() << endl;

    int num;
    cout << "Enter the number of row or column: ";
    cin >> num;
    cout << A[num] << endl;

    cout << "Enter a scalar: ";
    cin >> x;
    Matrix tmp = A * x;
    cout << "Temporary Matrix = Matrix #1 * scalar: " << endl;
    cout << tmp << endl;
    A *= x;
    cout << "Matrix #1 *= scalar:" << endl;
    cout << A << endl;

    cout << "Enter the sizes of Matrix #2: " << endl;
    int P, R;
    cin >> P >> R;
    Matrix B(P, R);
    cout << "Enter the values of Matrix #2: " << endl;
    for(int i = 0; i < P; i++)
        for(int j = 0; j < R; j++)
        {
            cin >> x;
            B.set(i, j, x);
        }
    cout << "Matrix #2:" << endl;
    cout << B << endl;

    cout << "A + B:" << endl;
    cout << A + B << endl;

    cout << "A += B: " << endl;
    A += B;
    cout << A << endl;

    cout << "A - B:" << endl;
    cout << A - B << endl;

    cout << "A -= B: " << endl;
    A -= B;
    cout << A << endl;

    cout << "A * B:" << endl;
    cout << A * B << endl;

    cout << "A *= B: " << endl;
    A *= B;
    cout << A << endl;

    cout << "-A:" << endl;
    cout << -A << endl;

    cout << "Enter the values of Matrix #1 again: " << endl;
    for(int i = 0; i < A.rows(); i++)
        for(int j = 0; j < A.columns(); j++)
        {
            cin >> x;
            A.set(i, j, x);
        }
    cout << "Enter the values of Matrix #2 again: " << endl;
    for(int i = 0; i < B.rows(); i++)
        for(int j = 0; j < B.columns(); j++)
        {
            cin >> x;
            B.set(i, j, x);
        }
    if(A == B)
    {
        cout << "The Matrixes #1 and #2 are equal!" << endl;
    }
    if(A != B)
    {
        cout << "The Matrixes #1 and #2 are different!" << endl;
    }
    
    cout << "The Vertical Concatenation of Matrix #1 and Identity Matrix: " << endl;
    cout << (A | identity(N)) << endl;

    cout << "Enter the diagonal values for horizontal concatenation: " << endl;
    double *values = new double [B.columns()];
    for(int i = 0; i < B.columns(); i++)
        cin >> values[i];
    cout << "The Horizontal Concatenation of Matrix #2 and Diagonal Matrix: " << endl;
    cout << (B / diagonal(values, B.columns())) << endl;
    return 0;
}